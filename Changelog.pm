# 0.8.7

- Add adv. settings parameter to customize the time before enabling multiple selection
- Fix js error on B2G 2.1

# 0.8.6

- Fix app build (sdcard detection error causing the app to crash on start)

# 0.8.5

- Fix invalid sdcard detection

# 0.8.4

- Fix Sync upload task does not works

# 0.8.3

- Fix for Z3C sdcard/sdcard1 names
- Fix delete directory in sdcard

# 0.8.2

- Fix selection of sdcard on some devices

# 0.8.1

- Update share link method to provide more apps

# 0.8.0

- Better gallery zoomin/zoomout move
- Remove items from selection when deleting them
- Refresh link token,  before opening external link (google)

# 0.7.0

- Google: Handle file/directory permissions  and create share links
- Dropbox: Create share link
- Webdav: Add share link based on webdav url
- Possiblity to create a new sdcard directory when selecting the download destination folder
- Display from / to message when downloading or upolading a file
- Do not fetch image gallery information if not necesarry

# 0.6.0

- Add one account per sdcard (instead of presenting sdcards as folders)
- New selection UX: long press to select folders and/or files
- Add the number of selected items
- Toggle option to open images with builtin gallery or with default app of the system
- Fix rename of directory in webdav
- Upload/Download directories now created new directories with the original folder name
- Dot not display modification date if not available
- Fix listing files on folders with special characters
- Fix Ftp Share function

# 0.5.0

- fix for internal storage for 'flame' like sdcards names: (sdcard/sdcard1).
- Load visible thumbnails of the files first, then next/previous thumbnails
- Disable download icon when clearing the queue.
- Fix file name in text viewer
- Fix issue with WebDAV and base URL having a tailing slash (Thks to  Snobili for the PR)
- Fix issue with WebDAV and HTML entities not being escaped
- Add opus audio mime type (and some more missing extensions)
- Fix upload from gallery app (no blob.name received)
- Fix thumbnail generation of files with some special characters

# 0.4.0

- Add FTP Support!
- Search in sdcards
- Clear cache after download queue is done
- New alert message if download queue is done with some errors
- Fix get Quota when refreshing token on google drive
- Fix image mime type (bug when opening images on non-owncloud webdavs)
- Fix Clickable files in view list mode
- Fix sound mime types
- Add ogg mime type for webdav/ftp

# 0.3.1

- Cancel previous search when opening a account
- Fix for fastmail webdav

# 0.3.0

- Add search for all apis minus webdav (todo?)
- Clear queue cancel current download too

# 0.2.2

- Fix box.com account login
- Fix open file on some file types

# 0.2.1

- Add depth parameter to webdav, to avoid error on some servers
- Fix error on webdav servers with no quota extension support


# 0.2.0

- Added a Upload / Download manager
- Added breadcrumb
- Change of google icons
- Added webdav account information
- Fix thumbnails size on owncloud/webdav (was forced to be squared before)
- Fix available file size for sdcards
- Choose between alternative link and open as text, if file not recognised

# 0.1.1

- Add account information (used vs available quota)
- Add txt/wav/mp3 mime types icons
- Add icons in preview container if no preview available
- Fix local file manager with phones with only 1 sdcard

# 0.1.0

- Local files thumbnails
- Browse local files directly from the app
- Share multiple files
- Better text reader 
- Fix download of big files
- Receive files from share function
- The cache images does not appear anymore on the gallery. You may need to clear the cache to remove old items.
- Clear cache option in settings
- Add donation link

# 0.0.7

- Explicit message when opening a file and the file is not supported by the phone.
- Fix copy/move to root directories
- Box.com api implementation
- Better thumbnail loading (3 by 3)
- Accounts list scrollbar if a lot of accounts created

# 0.0.5

- Change app icon
- File picker! compatible with google/dropbox/webdav 
- Onedrive (microsoft drive) integration
- Dropbox implementation
- Webdav/owncloud integration
- Fix download of google docs (using export links)

# 0.0.4

- Upload is now available for both internal or external cards.
- Enable/Disable cache images
- Choose on which sdcard put the cache files
- Display error if cache file creation fail
- Add warning about uploading individual files (thks mac from foxapps.eu)
- (ALPHA) Auto Sync folder - Upload or Download (every X minutes)

# 0.0.3

- Download folders and sub folders
- ru-RU translation
- better offline mode

# 0.0.2

- Fix settings github links

# 0.0.1

First commit
