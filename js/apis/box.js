var Box = function(){};
Box.prototype = new Apiprot();

Box.prototype.dbname='Box';
Box.prototype.icon='box';
Box.prototype.type='Box';
Box.prototype.clientid = '8jto29hr3y27staqyfgqtbxfrqmmxzjg';
Box.prototype.clientsecret = 'T2WERqhqpxLU0dUcWymPq5nloCQFQUYf';
Box.prototype.auth_url = 'https://app.box.com/api/oauth2/authorize';
Box.prototype.token_url = 'https://app.box.com/api/oauth2/token';
Box.prototype.callback_url ='https://localhost/callback.html';
Box.prototype.star_available=false;

Box.prototype._query = function(method,url,data, custom_header , custom_account, skip_401)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open(method, url, true);
        if(!data || typeof data ==='string')
        {
            r.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        }
        else
        {
            data = JSON.stringify(data);
            r.setRequestHeader("Content-type","application/json");
        }
        r.setRequestHeader("ts",new Date());
        if(custom_header)
        {
            r.responseType = "blob";
        }
        if(self.account|| custom_account)
        {
            r.setRequestHeader("authorization","Bearer "+(custom_account||self.account).access_token);
        }
        r.setRequestHeader("ts",new Date());

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status == 401 && url!=self.token_url && (custom_account||self.account) && !skip_401)
                {
                    console.error('401 ! refreshing token...', url, (custom_account||self.account).access_token);
                    self.refresh_token().then(function()
                    {
                        self._query(method,url,data, custom_header , custom_account, 1).then(ok, reject);
                    }, reject);
                }
                else if(r.status >= 200 && r.status< 400)
                {
                    return custom_header ? ok(r) :  ok(r.responseText);
                }
                else
                {
                    console.error('reject here ',r);
                    return reject(null);
                }
            }
        };
        r.send(data);
    });
};

Box.prototype.callback = function(url)
{
    if(!/state=boxcom/.test(url))
    {
        return;
    }
    var self=this;
    files.alert(translate('getting_account_access'));
    var reCode = /code=([^&]+)/;
    var reError = /error=([^&]+)/;
    if((result = url.match(reError)))
    {
        files.alert(translate('error_get_token'));
    }
    else if((result = url.match(reCode)))
    {
        var code=result[1];
        var data  = 'code='+encodeURIComponent(code)+'&';
        data += 'client_id='+encodeURIComponent(this.clientid)+'&';
        data += 'client_secret='+encodeURIComponent(this.clientsecret)+'&';
        data += 'redirect_uri='+encodeURIComponent(this.callback_url)+'&';
        data += 'state=boxcom&';
        data += 'grant_type='+encodeURIComponent('authorization_code');

        this.getToken(data).
            then(function()
            {
            }, function()
            {
                files.alert(translate('error_get_token'));
            });
    }
};

Box.prototype.getToken = function(data)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        files.alert(translate('getting_token'));
        self._query.bind(self)("POST", self.token_url, data)
        .then(function(text)
        {
            var data = JSON.parse(text);
            files.alert(translate('creating_account'));
            self.account=data;
            self.getProfile()
            .then(self.create_account_received.bind(self,data.access_token.replace(':'+self.clientid,''), data.refresh_token), reject)
            .then(ok, reject);
        }, reject);
    });
};

Box.prototype.getProfile = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        self._query.bind(self)("GET", '  http://api.box.com/2.0/users/me', null)
        .then(function(text)
        {
            var data = JSON.parse(text);
            if(data)
            {
                self.userid = data.id;
                self.image = null;
                self.email = data.login;
                ok();
            }
            else
            {
                reject();
            }
        }, function(err)
        {
            console.error('error get profile! ',err,this);
            reject();
        });
    });
};

Box.prototype.create_account_received = function(access_token, refresh_token)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'accounts' ], 'readwrite');
        //Create the Object to be saved i.e. our Note
        var value = {};
        value.access_token = access_token;
        value.refresh_token = refresh_token;
        value.userid = self.userid;
        value.email = self.email;
        value.id = self.userid;
        value.type = 'box';

        var accounts_trans = transaction.objectStore('accounts');
        var request = accounts_trans.put(value);
        request.onsuccess = function (e) {
            accounts.add(value,self);
            ok();
        };
        request.onerror = function (e) {
            console.error('creating account error');
            reject();
        };
    });
};

Box.prototype.create_account = function()
{
    var url = this.auth_url+'?'+
            'response_type=code&'+
            'client_id='+encodeURIComponent(this.clientid)+'&'+
            'redirect_uri='+encodeURIComponent(this.callback_url)+'&'+
            'state=boxcom&'
    ;
    window.open(url);
};

Box.prototype.refresh_token = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'accounts' ]);
        var dbaccounts = transaction.objectStore('accounts');

        var promises =[];
        // open a cursor to retrieve all items from the 'notes' store
        dbaccounts.openCursor().onsuccess = function (e) {
            var cursor = e.target.result;
            if (cursor) {
                var account = cursor.value;
                var data  = 'refresh_token='+encodeURIComponent(account.refresh_token)+'&';
                data += 'client_id='+encodeURIComponent(self.clientid)+'&';
                data += 'client_secret='+encodeURIComponent(self.clientsecret)+'&';
                data += 'grant_type='+encodeURIComponent('refresh_token');

                var promise = self._query.bind(self)("POST", self.token_url, data)
                .then(function(text)
                {
                    var data = JSON.parse(text);
                    if(self.account && account.id == self.account.id)
                    {
                        // Refresh 
                        self.account = account;
                    }
                    self.update_account(account,data.access_token, data.refresh_token)
                        .then(function()
                        {
                        }, function(err)
                        {
                            console.error('error refresh token ',err);
                        });
                }, function(err)
                {
                    console.error('error  update token',err,this);
                    reject();
                });
                promises.push(promise);
                cursor.continue();
            }
            else
            {
                Promise.all(promises).then(ok,  reject);
            }
        };
    });
};

Box.prototype.update_account = function(obj,token, refresh_token)
{
    var account = null;
    var self=this;

    return new Promise(function(ok, reject)
    {
        var objectStore = self.db.transaction(["accounts"], "readwrite").objectStore("accounts");
        var request = objectStore.get(obj.id);
        request.onerror = reject;
        request.onsuccess = function(event) {
            var data = request.result;
            data.access_token = token;
            data.refresh_token = refresh_token;

            // Put this updated object back into the database.
            var requestUpdate = objectStore.put(data);
            requestUpdate.onerror = reject;
            requestUpdate.onsuccess = ok;
        };
    });
};

Box.prototype.list_dir = function(id , pageToken)
{
    var self=this;
    var num_page = 1000;
    return new Promise(function(ok, reject)
    {
        if(!pageToken)
        {
            pageToken=0;
        }
        id=id.replace(self.account.id+'_','');
        var fetchid = id;

        if(id=='/' || id=='root')
        {
            fetchid='0';
        }

        var query;
        var url =  'https://api.box.com/2.0/folders/'+encodeURIComponent(fetchid)+'/items?fields=shared_link,name,created_at,modified_at,parent,id,type&limit='+num_page;
        url+='&offset='+(pageToken*num_page);

        self._query.bind(self)("GET", url, null)
        .then(function(text)
        {
            var data = JSON.parse(text);
            var result={
                pageToken : data.nextPageToken,
                items: []
            };
            var files=[];
            var dirs=[];
            for(var i=0; i<data.entries.length; i++)
            {
                var item = data.entries[i];

                var created_item =   self.convertItem(id,item);
                if(created_item.is_folder)
                {
                    dirs.push(created_item);
                }
                else
                {
                    files.push(created_item);
                }
            }
            result.items = Array.concat(dirs, files);

            // Get next page, if not trash bin
            if(data.entries>0)
            {
                pageToken++;
                self.list_dir(id , pageToken).then(function(subresults)
                {
                    result.items = result.items.concat(subresults.items);
                    ok(result);
                }, reject);
            }
            else
            {
                ok(result);
            }
        },function(err)
        {
            console.error('error fetching! ',url,err);
            reject();
        });
    });
};

Box.prototype.file_info = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        var url =  'https://api.box.com/2.0/files/'+encodeURIComponent(id)+'/?fields=shared_link,name,created_at,modified_at,parent,id,type';
        self._query.bind(self)("GET", url, null)
        .then(function(text)
        {
            var item = JSON.parse(text);
            var parent= id;
            item = self.convertItem(parent,item);
            ok(item);
        }, reject);
    });
};

Box.prototype.rename = function(item, newname, is_folder)
{
    var self=this;
    var id = item.id;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        if(is_folder)
        {
            self._query("PUT",  'https://api.box.com/2.0/folders/'+id, { name: newname }).then(ok, reject);
        }
        else
        {
            self._query("PUT",  'https://api.box.com/2.0/files/'+id, { name: newname }).then(ok, reject);
        }
    });
};

Box.prototype.delete = function(item, is_folder)
{
    var self=this;
    var id = item.id;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        if(is_folder)
        {
            self._query("DELETE",  'https://api.box.com/2.0/folders/'+id, null).then(ok, reject);
        }
        else
        {
            self._query("DELETE",  'https://api.box.com/2.0/files/'+id, null).then(ok, reject);
        }
    });
};

Box.prototype.mkdir = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        if(id=='/')
        {
            id='0';
        }
        self._query("POST",  'https://api.box.com/2.0/folders', {parent: { id: id}, name: newname })
        .then(function(text)
        {
            var received = JSON.parse(text);
            ok(received);
        }, function(err)
        {
            reject();
            console.warn('error mkdir '+err);
        });
    });
};

Box.prototype.move = function(id, destination, is_folder)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    if(id=='/') { id = '0'; }

    destination=destination.replace(self.account.id+'_','');
    if(destination=='/' || destination=='root') { destination = '0'; }
    return new Promise(function(ok, reject)
    {
        if(is_folder)
        {
            self._query("PUT",  'https://api.box.com/2.0/folders/'+id, { parent: { id: destination} }).then(ok, reject);
        }
        else
        {
            self._query("PUT",  'https://api.box.com/2.0/files/'+id, { parent: {id: destination} }).then(ok, reject);
        }
    });
};

Box.prototype.copy = function(id, destination, is_folder)
{
    var self=this;
    id=id.replace(self.account.id+'_','');

    destination=destination.replace(self.account.id+'_','');
    if(destination=='/' || destination=='root') { destination = '0'; }

    return new Promise(function(ok, reject)
    {
        if(is_folder)
        {
            self._query("POST",  'https://api.box.com/2.0/folders/'+id+'/copy', { parent: { id: destination} }).then(ok, reject);
        }
        else
        {
            self._query("POST",  'https://api.box.com/2.0/files/'+id+'/copy', { parent: {id: destination} }).then(ok, reject);
        }
    });
};

Box.prototype.create = function(id,blob)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        if(id=='/') { id = '0'; }

        // prepare data
        blob.name = blob.name.replace(/.*\//,'');
        var form = new FormData();
        form.append('file',blob);
        form.append('parent_id',id);

        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open('POST', 'https://upload.box.com/api/2.0/files/content', true);


        r.setRequestHeader("authorization","Bearer "+self.account.access_token);
        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >=200 && r.status<400)
                {
                    ok();
                }
                else
                {
                    return reject(r.responseText);
                }
            }
        };
        r.send(form);
    });
};

Box.prototype.convertItem = function(id,item)
{
    var self=this;
    var filename = decodeURIComponent(item.name);
    var is_dir = item.type=='folder';
    var mime;
    var icon;

    if(is_dir)
    {
        mime = translate('folder');
        icon='folder';
    }
    else
    {
        mimeData = this.getMimeAndIcon(filename);
        mime = mimeData.mime;
        icon=mimeData.icon;
    }

    var created=
    {
        parent: self.account.id+'_'+id,
        real_parent: id,
        id: self.account.id+'_'+item.id,
        realid: item.id,
        icon: 'img/icons/dropbox/'+(icon)+'.gif',
        name: item.name,
        mime: mime,
        creation_date: (new Date(item.created_at)).getTime(),
        modified_date: (new Date(item.modified_at)).getTime(),
        size: item.fileSize || null,
        thumbnail: !is_dir ? 'https://api.box.com/2.0/files/'+encodeURIComponent(item.id)+'/thumbnail.png?max_width=200&max_height=200' : null,
        bigthumbnail: !is_dir ? 'https://api.box.com/2.0/files/'+encodeURIComponent(item.id)+'/thumbnail.png?max_width=800&max_height=800' : null,
        download: 'https://api.box.com/2.0/files/'+encodeURIComponent(item.id)+'/content',
        webDownload: null,
        alternateLink: null,

        is_folder:  is_dir,

        is_starred:  false,
        is_hidden:  false,
        is_trashed:  false,
    };
    console.log('convert ',item);
    if(item.shared_link)
    {
        created.shared_link = item.shared_link.url;
    }

    if(!created.download)
    {
        if(item.exportLinks)
        {
            // Prefered mime types
            var prefered=new RegExp('vnd.openxmlformats');
            // @TODO: choose from which export link download.
            var keys= Object.keys(item.exportLinks);
            var selected_index =0;
            for(var i=0; i<keys.length;i++)
            {
                if(prefered.test(keys[i]))
                {
                    selected_index=i;
                }
            }
            if(item.exportLinks[keys[selected_index]].indexOf('exportFormat=')>-1)
            {
                created.name +=  "."+item.exportLinks[keys[selected_index]].replace(/.*=/,'');
            }
            created.download = item.exportLinks[keys[0]];
        }
    }
    return created;
};

Box.prototype.getInfo = function(account)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        self._query.bind(self)("GET", 'http://api.box.com/2.0/users/me', null, null, account)
        .then(function(text)
        {
            var data = JSON.parse(text);
            if(data)
            {
                var used = files.getReadableFileSizeString(parseInt(data.space_used,10));
                var total = files.getReadableFileSizeString(parseInt(data.space_amount,10));
                ok(used+' / '+total);
            }
            else
            {
                reject();
            }

        }, function(err)
        {
            console.error('error get profile! ',err,this);
            reject();
        });
    });
};


Box.prototype.search = function(search)
{
    var self=this;
    var num_page = 1000;
    return new Promise(function(ok, reject)
    {
        var query;
        var url =  'https://api.box.com/2.0/search?query='+encodeURIComponent(search)+'&fields=shared_link,name,created_at,modified_at,parent,id,type&limit='+num_page;

        self._query.bind(self)("GET", url, null)
        .then(function(text)
        {
            var data = JSON.parse(text);
            var result={
                pageToken : null,
                items: []
            };
            var files=[];
            var dirs=[];
            for(var i=0; i<data.entries.length; i++)
            {
                var item = data.entries[i];

                var created_item =   self.convertItem('search',item);
                if(created_item.is_folder)
                {
                    dirs.push(created_item);
                }
                else
                {
                    files.push(created_item);
                }
            }
            result.items = Array.concat(dirs, files);
            ok(result);

        },function(err)
        {
            console.error('error fetching! ',url,err);
            reject();
        });
    });
};

Box.prototype.apishare = function(item)
{
    var self=this;

    console.log('item ',item);
    if(this.apishare_selector)
    {
        this.apishare_selector.close();
    }
    var fileid=item.id.replace(self.account.id+'_','');

    this.apishare_selector = new Selector();

    this.file_info(item.id).then(function(item)
    {
        var div = document.createElement('div');

        var ul = document.createElement('ul');
        ul.className='permissions';
        div.appendChild(ul);
        var li = document.createElement('li');
        li.innerHTML=translate('loading');
        ul.appendChild(li);

        // reset list of permissions
        ul.innerHTML='';
        var has_link = false;

        var buttons = [];

        if(item.shared_link)
        {
            li = document.createElement('li');

            var div_edit =  document.createElement('div');
            div_edit.className='perm_edit';
            li.appendChild(div_edit);
            
            span = document.createElement('span');
            span.className='fa fa-remove';
            div_edit.appendChild(span);
            span.addEventListener('click',  self.perm_delete.bind(self, li,item));

            var div_name =  document.createElement('div');
            div_name.className='perm_name';
            var txt = document.createTextNode(translate('permission_role_reader'));
            div_name.appendChild(txt);
            li.appendChild(div_name);

            var div_url =  document.createElement('div');
            var input =  document.createElement('a');
            div_url.className='perm_url';
            input.setAttribute('target','_blank');
            input.setAttribute('href',item.shared_link);
            input.innerHTML=item.shared_link;
            div_url.appendChild(input);


            input =  document.createElement('a');
            input.className='bb-button bb-recommend';
            input.addEventListener('click', function()
            {
                console.log('new activity!');
                new MozActivity({
                    name: "share",
                    data: {
                        type: "url",
                        number: 1,
                        url: item.shared_link
                    }    
                });
            });
            input.innerHTML=translate('item_share');
            div_url.appendChild(input);

            li.appendChild(div_url);

            ul.appendChild(li);
        }
        else
        {
            buttons.push({
                'text':  translate('permission_create_link_reader'),
                'icon':  'plus',
                'autoclose': false,
                'callback':  self.perm_add_link.bind(self, item)
            });
        }

        self.apishare_selector.create('permission_container',translate('share_link'), div, buttons);
    });

};

Box.prototype.perm_delete = function(li, item)
{
    var self=this;
    var id = item.id;
    id=id.replace(self.account.id+'_','');

    if(!confirm(translate('permission_confirm_delete')))
    {
        return Promise.reject();
    }
    return new Promise(function(ok, reject)
    {
        if(item.is_folder)
        {
            self._query("PUT",  'https://api.box.com/2.0/folders/'+id, { shared_link: null }).then(function()
            {
                self.apishare(item);
                ok();
            }, reject);
        }
        else
        {
            self._query("PUT",  'https://api.box.com/2.0/files/'+id, { shared_link: null }).then(function()
            {
                self.apishare(item);
                ok();
            }, reject);
        }
    });
};

Box.prototype.perm_add_link = function(item)
{
    var self=this;
    var fileid=item.id.replace(self.account.id+'_','');

    return new Promise(function(ok, reject)
    {
        if(item.is_folder)
        {
            self._query("PUT",  'https://api.box.com/2.0/folders/'+fileid, { shared_link: { access: 'open' } }).then(function()
            {
                self.apishare(item);
                ok();
            }, reject);
        }
        else
        {
            self._query("PUT",  'https://api.box.com/2.0/files/'+fileid, { shared_link: { access: 'open' } }).then(function()
            {
                self.apishare(item);
                ok();
            }, reject);
        }
    });
};
