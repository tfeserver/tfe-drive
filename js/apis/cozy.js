var Cozy = function(){};
Cozy.prototype = new Apiprot();

Cozy.prototype.dbname='Cozy';
Cozy.prototype.icon='cozy';
Cozy.prototype.type='Cozy';
Cozy.prototype.star_available = false;

Cozy.prototype._query = function(method,url,data, custom_header , custom_account)
{
    url = url.replace('#','%23');
    url = url.replace(/\/+/g,'/');

    var self=this;
    return new Promise(function(ok, reject)
    {
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open(method, url, true);
        if(method==='MOVE' || method==='COPY')
        {
            data = data.replace(/\/+/g,'/');
            r.setRequestHeader("Destination",data);
        }
        else if(!data || typeof data ==='string')
        {
            r.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        }
        else
        {
            data = JSON.stringify(data);
            r.setRequestHeader("Content-type","application/json");
        }
        r.setRequestHeader("ts",new Date());
        if(custom_header)
        {
            r.responseType = "blob";
        }
        if(self.account || custom_account)
        {
            var account = custom_account|| self.account;
            r.setRequestHeader("Authorization","Basic "+btoa(account.username+":"+account.password));
        }
        r.setRequestHeader("ts",new Date());

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >= 200 && r.status< 400)
                {
                    return custom_header ? ok(r) :  ok(r.responseText);
                }
                else
                {
                    return reject(null);
                }
            }
        };
        r.send(data);
    });
};

Cozy.prototype.setAccount = function(id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var objectStore = self.db.transaction(["accounts"], "readwrite").objectStore("accounts");
        var request = objectStore.get(id);
        request.onerror = reject;
        request.onsuccess = function(event) {
            var data = request.result;
            self.account= data;
            self.account.prefix = self.account.url.replace(/.*:\/\/[^\/]+/,'');
            ok();
        };
    });
};

Cozy.prototype.create_account_received = function(appName,name, url, username, password)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'accounts' ], 'readwrite');
        //Create the Object to be saved i.e. our Note
        var value = {};
        value.appName = appName;
        value.name = name;
        value.url = url;
        value.username = username;
        value.password = password;
        value.type = 'cozy';
        value.id = url+username;

        // To display account name
        value.email = name;

        var accounts_trans = transaction.objectStore('accounts');
        var request = accounts_trans.put(value);
        request.onsuccess = function (e) {
            accounts.add(value,self);
            ok();
        };
        request.onerror = function (e) {
            console.error('creating account error');
            reject();
        };
    });
};

Cozy.prototype.create_account = function()
{
    this.create_selector = new Selector();

    var description=document.createElement('div');

    var p = document.createElement('p');
    p.innerHTML=translate('cozy_help');
    description.appendChild(p);

    var form = document.createElement('form');
    description.appendChild(form);

    p = document.createElement('p');
    p.className='line_input';
    form.appendChild(p);


    ['webdav_name','webdav_url','webdav_username','webdav_password'].forEach(function(field)
    {
        var label = document.createElement('label');
        label.setAttribute('for',field);
        label.innerHTML=translate(field);
        p.appendChild(label);
        var input = document.createElement('input');
        if(field==='webdav_password') { input.setAttribute('type','password'); }
        input.setAttribute('class',field);
        input.setAttribute('id',field);
        p.appendChild(input);
    });

    var items=[];
            items.push({
                'text':  translate('cozy_creation'),
                'autoclose':false,
                'icon':  'plus',
                'callback':  this.create_form.bind(this)
            });

    this.create_selector.create(
            'selector_form',
            translate('cozy_creation'),
            description,
            items);
};

Cozy.prototype.create_form = function()
{
    var self=this;

    var emptys =[];
    ['webdav_name','webdav_url','webdav_username','webdav_password'].forEach(function(field)
    {
        var dom_field = document.querySelector('#'+field);
        if(!dom_field.value)
        {
            emptys.push(translate(field));
        }
    });

    if(emptys.length>0)
    {
        return files.alert(translate('empty_fields', { fields: emptys.join(', ')}));
    }

    var name  = document.querySelector('#webdav_name').value;
    var url  = document.querySelector('#webdav_url').value;
    var username = document.querySelector('#webdav_username').value;
    var password = document.querySelector('#webdav_password').value;

    files.alert(translate('webdav_checking'),6000);

    var username_encoded = encodeURIComponent(username);
    var password_encoded = encodeURIComponent(password);

    url = url.replace(/\/+/g,'/');
    url = url.replace(/\/$/,'');
    console.log('url ',url);

    // Test connection
    var r = new XMLHttpRequest({ mozSystem: true });
    r.open('POST',url+'/device');
    r.setRequestHeader("Authorization","Basic "+btoa(username+":"+password));
    r.setRequestHeader("Content-Type","application/json");

    var appName  = 'tfeDrive'+(Math.ceil(Math.random()*1000));

    r.onreadystatechange = function () {
        if (r.readyState == 4)
        {
            if(r.status >=200 && r.status<400)
            {
                console.log('ok ', r);
                var data = JSON.parse(r.responseText);
                console.log('data ',data);
                self.create_account_received(appName,name,url,appName,data.password);
                files.alert(translate('creating_account'));
                // All ok ok, close create selector
                self.create_selector.close();
            }
            else
            {
                files.alert(translate('webdav_invalid'));
            }
        }
    };
    r.send(JSON.stringify({"login": appName, "permissions": {"Folder": { "description": "Request api" }, "File": {"description": "Synchronize files"}} }));
};

Cozy.prototype.list_dir = function(id)
{
    var self=this;
    console.log('list dir',id);
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');

        var query;
        var url;
        var method='GET';
        var data=null;
        var path_id = id;

        if(id=='root' || id=='/')
        {
            id='';
        }

        var result={
            pageToken : null,
            items: []
        };
        var dirs=[];
        var files=[];


        var promises=[];
        promises.push(new Promise(function(subok, subreject) {
            url =  self.account.url+'/ds-api/request/folder/byfolder/';
            self._query('POST', url, { key: id})
            .then(function(text)
            {
                var data = JSON.parse(text);
                for(var i=0; i<data.length; i++)
                {
                    var item = data[i];
                    console.log('convertin ',item);

                    var created_item =   self.convertItem(id,item, true);
                    dirs.push(created_item);
                }
                subok();
            }, subreject);
        }));

        promises.push(new Promise(function(subok, subreject) {
            url =  self.account.url+'/ds-api/request/file/byfolder/';
            self._query('POST', url, { key: id})
            .then(function(text)
            {
                console.log('received text',text, this);
                var data = JSON.parse(text);
                for(var i=0; i<data.length; i++)
                {
                    var item = data[i];
                    console.log('convertin ',item);

                    var created_item =   self.convertItem(id,item, false);
                    files.push(created_item);
                }
                subok();
            }, subreject);
        }));

        Promise.all(promises).then(function()
        {
            result.items = Array.concat(dirs, files);
            console.log('result ',result.items);
            ok(result);
        }
        ,function(err)
        {
            console.error('error fetching! ',url,err);
            reject();
        });
    });
};

Cozy.prototype.file_info = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');

    url =  self.account.url+'/ds-api/request/file/byfullpath/';
    return new Promise(function(ok, reject)
    {
        self._query('POST', url, { key: id})
        .then(function(text)
        {
            console.log('received text',text, this);
            var data = JSON.parse(text);
            if(data.length===1)
            {
                for(var i=0; i<data.length; i++)
                {
                    var item = data[i];
                    var created_item =   self.convertItem(id,item, false);
                    console.log('item ',created_item);
                    ok(created_item);
                }
            }
            else
            {
                reject();
            }
        }, reject);
    });
    return new Promise(function(ok, reject)
    {
        url =  self.account.url+'apps/files/files/'+id+'/';
        self._query('GET', url)
        .then(function(text)
        {
            var nodes = text.querySelectorAll('response');
            if(nodes.length>0)
            {
                var parent= id;
                var item = self.convertItem(parent.replace(/[^\/]+\/?$/,''),nodes[0], false);
                ok(item);
            }
            else
            {
                reject();
            }
        }, reject);
    });
};

Cozy.prototype.rename = function(item, newname)
{
    var self=this;
    var id = item.id;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        console.log('new name ', id, newname);
        url =  self.account.url+'/ds-api/data/merge/'+encodeURIComponent(item.cozy_id)+'/';
        self._query("PUT",  url, { name: newname} )
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Cozy.prototype.delete = function(item)
{
    var self=this;
    var id = item.id;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        url =  self.account.url+'/ds-api/data/'+encodeURIComponent(item.cozy_id)+'/';
        self._query("DELETE",  url,null)
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Cozy.prototype.mkdir = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    if(id=='root' || id=='/')   { id=''; }

    return new Promise(function(ok, reject)
    {
        url =  self.account.url+'/ds-api/data/';
        self._query("POST", url, { docType:'folder', path:id, name: newname})
        .then(function(text)
        {
            ok({ id : id+'/'+newname });
        }, function(err)
        {
            reject();
            console.warn('error mkdir '+err);
        });
    });
};

Cozy.prototype.move = function(id, destination)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='root')
    {
        destination='/';
    }
    id=id.replace(/\/$/,'');

    return new Promise(function(ok, reject)
    {
        self._query("MOVE",  self.account.url+'/'+self.encode_path(id), self.account.prefix+'/'+self.encode_path(destination+'/'+id.replace(/^.*\//,'')))
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Cozy.prototype.copy = function(id, destination)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='root')
    {
        destination='/';
    }
    id=id.replace(/\/$/,'');

    return new Promise(function(ok, reject)
    {
        console.log('copy ! ',destination);
        self._query("COPY",  self.account.url+'/'+self.encode_path(id), self.account.prefix+'/'+self.encode_path(destination+'/'+id.replace(/^.*\//,'')))
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Cozy.prototype.create = function(id,blob)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open('PUT', self.account.url+'/'+self.encode_path(id+'/'+blob.name.replace(/^.*\//,''), ''));
        var boundary = (new Date()).getTime()+'_'+Math.ceil(Math.random()*10000);
        r.setRequestHeader("Content-Type","multipart/related; boundary=\""+boundary+"\"");
        r.setRequestHeader("Authorization","Basic "+btoa(self.account.username+":"+self.account.password));

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >= 200 && r.status< 400)
                {
                    return ok(r.responseText);
                }
                else
                {
                    console.error('here reject', r);
                    return reject(r.responseText);
                }
            }
        };
        r.send(blob);
    });
};

Cozy.prototype.convertItem = function(id,item, is_folder)
{
    var self=this;

    var filename = item.value.name;
    var modif = item.value.lastModification;

    filename = filename.replace(self.account.prefix, '');
    filename = filename.replace(/(^\/)/g,'');

    var mime;
    var is_dir = is_folder;
    var icon;
    if(is_dir)
    {
        mime = translate('folder');
        icon='folder';
    }
    else
    {
        mimeData = this.getMimeAndIcon(filename);
        mime = mimeData.mime;
        icon=mimeData.icon;
    }
    var has_thumb = item.value && item.value.binary && item.value.binary.thumb ? true : false;
    
    var created=
    {
        parent: self.account.id+'_'+id,
        real_parent: id,
        id: self.account.id+'_'+id+'/'+filename,
        realid: id+'/'+filename,
        cozy_id: item.id,
        icon: 'img/icons/dropbox/'+(icon)+'.gif',
        name: is_dir ? filename.replace(/^.*?([^\/]+)\/?$/,'$1') : filename.replace(/^.*\//,''),
        mime: mime,
        creation_date: 0,
        modified_date: modif,
        size: item.value.size,
        thumbnail: has_thumb ? self.account.url.replace(/\/$/, '')+'/ds-api/data/'+item.id+'/binaries/thumb' : null,
        bigthumbnail: has_thumb ? self.account.url.replace(/\/$/, '')+'/ds-api/data/'+item.id+'/binaries/thumb' : null,
        download: self.account.url.replace(/\/$/, '')+'/ds-api/data/'+item.id+'/binaries/file',
        webDownload: null,
        alternateLink: null,

        is_folder:  is_dir,
        is_starred:  false,
        is_hidden:  false,
        is_trashed: false,
    };
    return created;
};

Cozy.prototype.getInfo = function(account)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var url =  account.url+'/';
        reject();
    });
};

Cozy.prototype.encode_path = function(url)
{
    // encode uri components parts
    return url.split(/\/+/g).map(function(x)
    {
        return encodeURIComponent(x);
    }).join('/');
};

Cozy.prototype.apishare = function(item)
{
    var self=this;

    if(this.apishare_selector)
    {
        this.apishare_selector.close();
    }
    var fileid=item.id.replace(self.account.id+'_','');

    this.apishare_selector = new Selector();
    var div = document.createElement('div');
    var p = document.createElement('p');
    p.innerHTML=translate('webdav_link_warning');
    div.appendChild(p);

    var div_url =  document.createElement('div');

    var input =  document.createElement('a');
    div_url.className='perm_url';
    input.setAttribute('href',item.download);
    input.setAttribute('target','_blank');
    input.innerHTML=item.download;
    div_url.appendChild(input);

    input =  document.createElement('a');
    input.className='bb-button bb-recommend';
    input.addEventListener('click', function()
    {
        console.log('new activity!');
        new MozActivity({
            name: "share",
            data: {
                type: "url",
                number: 1,
                url: item.download
            }    
        });
    });
    input.innerHTML=translate('item_share');
    div_url.appendChild(input);

    div.appendChild(div_url);
    self.apishare_selector.create('permission_container',translate('share_link'), div, []);
};
