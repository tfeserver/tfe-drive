var Webdav = function(){};
Webdav.prototype = new Apiprot();

Webdav.prototype.dbname='Webdav';
Webdav.prototype.icon='webdav';
Webdav.prototype.type='Webdav';
Webdav.prototype.star_available = false;

Webdav.prototype._query = function(method,url,data, custom_header , custom_account)
{
    url = url.replace('#','%23');
    url = url.replace(/\/+/g,'/');

    var self=this;
    return new Promise(function(ok, reject)
    {
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open(method, url, true);
        if(method==='PROPFIND')
        {
            r.setRequestHeader("Depth",1);
        }
        else if(method==='MOVE' || method==='COPY')
        {
            data = data.replace(/\/+/g,'/');
            r.setRequestHeader("Destination",data);
        }
        else if(!data || typeof data ==='string')
        {
            r.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        }
        else
        {
            data = JSON.stringify(data);
            r.setRequestHeader("Content-type","application/json");
        }
        r.setRequestHeader("ts",new Date());
        if(custom_header)
        {
            r.responseType = "blob";
        }
        if(self.account || custom_account)
        {
            var account = custom_account|| self.account;
            r.setRequestHeader("Authorization","Basic "+btoa(account.username+":"+account.password));
        }
        r.setRequestHeader("ts",new Date());

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >= 200 && r.status< 400)
                {
                    return custom_header ? ok(r) :  ok(r.responseXML);
                }
                else
                {
                    return reject(null);
                }
            }
        };
        r.send(data);
    });
};

Webdav.prototype.setAccount = function(id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var objectStore = self.db.transaction(["accounts"], "readwrite").objectStore("accounts");
        var request = objectStore.get(id);
        request.onerror = reject;
        request.onsuccess = function(event) {
            var data = request.result;
            self.account= data;
            self.account.prefix = self.account.url.replace(/.*:\/\/[^\/]+/,'');
            if(self.account.owncloud)
            {
                self.account.preview_url = self.account.url.replace('remote.php/webdav','index.php/core/preview.png?file=%file%&x=200&y=200&a=true&scalingup=0&forceIcon=0');
                self.account.bigpreview_url = self.account.url.replace('remote.php/webdav','index.php/core/preview.png?file=%file%&x=800&y=800&a=true&scalingup=0&forceIcon=0');
            }
            ok();
        };
    });
};

Webdav.prototype.create_account_received = function(name, url, username, password)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'accounts' ], 'readwrite');
        //Create the Object to be saved i.e. our Note
        var value = {};
        value.name = name;
        value.url = url;
        value.username = username;
        value.password = password;
        value.type = 'webdav';
        value.id = url+username;
        value.owncloud = url.indexOf('remote.php/webdav')!==-1;

        // To display account name
        value.email = name;

        var accounts_trans = transaction.objectStore('accounts');
        var request = accounts_trans.put(value);
        request.onsuccess = function (e) {
            accounts.add(value,self);
            ok();
        };
        request.onerror = function (e) {
            console.error('creating account error');
            reject();
        };
    });
};

Webdav.prototype.create_account = function()
{
    this.create_selector = new Selector();

    var description=document.createElement('div');

    var p = document.createElement('p');
    p.innerHTML=translate('webdav_help');
    description.appendChild(p);

    var form = document.createElement('form');
    description.appendChild(form);

    p = document.createElement('p');
    p.className='line_input';
    form.appendChild(p);


    ['webdav_name','webdav_url','webdav_username','webdav_password'].forEach(function(field)
    {
        var label = document.createElement('label');
        label.setAttribute('for',field);
        label.innerHTML=translate(field);
        p.appendChild(label);
        var input = document.createElement('input');
        if(field==='webdav_password') { input.setAttribute('type','password'); }
        input.setAttribute('class',field);
        input.setAttribute('id',field);
        p.appendChild(input);
    });

    var items=[];
            items.push({
                'text':  translate('webdav_create'),
                'autoclose':false,
                'icon':  'plus',
                'callback':  this.create_form.bind(this)
            });

    this.create_selector.create(
            'selector_form',
            translate('webdav_creation'),
            description,
            items);
};

Webdav.prototype.create_form = function()
{
    var self=this;

    var emptys =[];
    ['webdav_name','webdav_url','webdav_username','webdav_password'].forEach(function(field)
    {
        var dom_field = document.querySelector('#'+field);
        if(!dom_field.value)
        {
            emptys.push(translate(field));
        }
    });

    if(emptys.length>0)
    {
        return files.alert(translate('empty_fields', { fields: emptys.join(', ')}));
    }

    var name  = document.querySelector('#webdav_name').value;
    var url  = document.querySelector('#webdav_url').value;
    var username = document.querySelector('#webdav_username').value;
    var password = document.querySelector('#webdav_password').value;

    files.alert(translate('webdav_checking'),6000);

    // Test connection
    var r = new XMLHttpRequest({ mozSystem: true });
    r.open('PROPFIND',url);
    r.setRequestHeader("Authorization","Basic "+btoa(username+":"+password));
    r.setRequestHeader("Depth",1);

    r.onreadystatechange = function () {
        if (r.readyState == 4)
        {
            if(r.status >=200 && r.status<400)
            {
                self.create_account_received(name,url,username,password);
                files.alert(translate('creating_account'));
                // All ok ok, close create selector
                self.create_selector.close();
            }
            else
            {
                files.alert(translate('webdav_invalid'));
            }
        }
    };
    r.send();
};

Webdav.prototype.list_dir = function(id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');

        var query;
        var url;
        var method='GET';
        var data=null;
        var path_id = id;

        if(id=='root')
        {
            id='/';
        }
        url =  self.account.url+'/'+self.encode_path(id);

        self._query.bind(self)('PROPFIND', url, null)
        .then(function(text)
        {
            var nodes = text.querySelectorAll('response');
            var result={
                items: []
            };
            var files=[];
            var dirs=[];
            for(var i=0; i<nodes.length; i++)
            {
                var item = nodes[i];

                var created_item =   self.convertItem(id,item);
                if(created_item)
                {
                    if(created_item.is_folder)
                    {
                        dirs.push(created_item);
                    }
                    else
                    {
                        files.push(created_item);
                    }
                }
            }
            result.items = Array.concat(dirs, files);
            ok(result);
        },function(err)
        {
            console.error('error fetching! ',url,err);
            reject();
        });
    });
};

Webdav.prototype.file_info = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        var url =  self.account.url+'/'+self.encode_path(id);

        self._query.bind(self)('PROPFIND', url, null)
        .then(function(text)
        {
            var nodes = text.querySelectorAll('response');
            if(nodes.length>0)
            {
                var parent= id;
                var item = self.convertItem(parent.replace(/[^\/]+\/?$/,''),nodes[0]);
                ok(item);
            }
            else
            {
                reject();
            }
        }, reject);
    });
};

Webdav.prototype.rename = function(item, newname)
{
    var self=this;
    var id = item.id;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        console.log('new name ', id, newname);
        self._query("MOVE",  self.account.url+'/'+self.encode_path(id), self.account.prefix+'/'+self.encode_path(id.replace(/[^\/]+\/?$/,newname)))
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Webdav.prototype.delete = function(item)
{
    var self=this;
    var id = item.id;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("DELETE",  self.account.url+'/'+self.encode_path(id), null)
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Webdav.prototype.mkdir = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    //if(id=='root')   { id=''; }

    return new Promise(function(ok, reject)
    {
        self._query("MKCOL", self.account.url+'/'+self.encode_path(id+'/'+newname+'/'))
        .then(function(text)
        {
            ok({ id : id+'/'+newname });
        }, function(err)
        {
            reject();
            console.warn('error mkdir '+err);
        });
    });
};

Webdav.prototype.move = function(id, destination)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='root')
    {
        destination='/';
    }
    id=id.replace(/\/$/,'');

    return new Promise(function(ok, reject)
    {
        self._query("MOVE",  self.account.url+'/'+self.encode_path(id), self.account.prefix+'/'+self.encode_path(destination+'/'+id.replace(/^.*\//,'')))
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Webdav.prototype.copy = function(id, destination)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='root')
    {
        destination='/';
    }
    id=id.replace(/\/$/,'');

    return new Promise(function(ok, reject)
    {
        console.log('copy ! ',destination);
        self._query("COPY",  self.account.url+'/'+self.encode_path(id), self.account.prefix+'/'+self.encode_path(destination+'/'+id.replace(/^.*\//,'')))
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Webdav.prototype.create = function(id,blob)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open('PUT', self.account.url+'/'+self.encode_path(id+'/'+blob.name.replace(/^.*\//,''), ''));
        var boundary = (new Date()).getTime()+'_'+Math.ceil(Math.random()*10000);
        r.setRequestHeader("Content-Type","multipart/related; boundary=\""+boundary+"\"");
        r.setRequestHeader("Authorization","Basic "+btoa(self.account.username+":"+self.account.password));

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >= 200 && r.status< 400)
                {
                    return ok(r.responseText);
                }
                else
                {
                    console.error('here reject', r);
                    return reject(r.responseText);
                }
            }
        };
        r.send(blob);
    });
};

Webdav.prototype.convertItem = function(id,item)
{
    var self=this;

    var filename = decodeURIComponent(item.querySelector('href').innerHTML);
    var modif = (new Date(item.querySelector('getlastmodified').innerHTML)).getTime();
    var size_container = item.querySelector('getcontentlength');
    var type = item.querySelector('getcontenttype');

    filename = filename.replace(self.account.prefix, '');
    filename = filename.replace(/(^\/)/g,'');

    if(filename===id || filename==id+'/' || filename=='')
    {
        return null;
    }

    var mime;
    var is_dir = !size_container || (type && type.innerHTML=='httpd/unix-directory') ;
    var icon;
    if(is_dir)
    {
        mime = translate('folder');
        icon='folder';
    }
    else
    {
        mimeData = this.getMimeAndIcon(filename);
        mime = mimeData.mime;
        icon=mimeData.icon;
    }
    
    var created=
    {
        parent: self.account.id+'_'+id,
        real_parent: id,
        id: self.account.id+'_'+filename,
        realid: filename,
        icon: 'img/icons/dropbox/'+(icon)+'.gif',
        name: is_dir ? filename.replace(/^.*?([^\/]+)\/?$/,'$1') : filename.replace(/^.*\//,''),
        mime: mime,
        creation_date: 0,
        modified_date: modif,
        size: size_container ? size_container.innerHTML : 0,
        thumbnail: self.account.owncloud ? self.account.preview_url.replace('%file%', encodeURIComponent(filename)) : null,
        bigthumbnail: self.account.owncloud ? self.account.bigpreview_url.replace('%file%', encodeURIComponent(filename)) : null,
        download: self.account.url.replace(/\/$/, '')+'/'+filename,
        webDownload: null,
        alternateLink: null,

        is_folder:  is_dir,
        is_starred:  false,
        is_hidden:  false,
        is_trashed: false,
    };
    if(!created.downloadUrl)
    {
        if(created.exportLinks)
        {
            // @TODO: choose from which export link download.
            var keys= Object.keys(created.exportLinks);
            created.downloadUrl = created.exportLinks[keys[0]];
        }
    }
    return created;
};

Webdav.prototype.getInfo = function(account)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var url =  account.url+'/';
        self._query("PROPFIND", url, null, null, account)
        .then(function(text)
        {
            var nodes = text.querySelectorAll('response');
            if(nodes.length>0)
            {
                var item = nodes[0];
                if(item.querySelector('quota-used-bytes'))
                {
                    var used = parseInt(item.querySelector('quota-used-bytes').innerHTML,10);
                    var available = parseInt(item.querySelector('quota-available-bytes').innerHTML,10);
                    ok(files.getReadableFileSizeString(used)+' / '+files.getReadableFileSizeString(available+used));
                }
                else
                {
                    reject();
                }
            }
            else
            {
                reject();
            }
        }, reject)  
    });
};

Webdav.prototype.encode_path = function(url)
{
    // encode uri components parts
    return url.split(/\/+/g).map(function(x)
    {
        return encodeURIComponent(x);
    }).join('/');
};

Webdav.prototype.apishare = function(item)
{
    var self=this;

    if(this.apishare_selector)
    {
        this.apishare_selector.close();
    }
    var fileid=item.id.replace(self.account.id+'_','');

    this.apishare_selector = new Selector();
    var div = document.createElement('div');
    var p = document.createElement('p');
    p.innerHTML=translate('webdav_link_warning');
    div.appendChild(p);

    var div_url =  document.createElement('div');

    var input =  document.createElement('a');
    div_url.className='perm_url';
    input.setAttribute('href',item.download);
    input.setAttribute('target','_blank');
    input.innerHTML=item.download;
    div_url.appendChild(input);

    input =  document.createElement('a');
    input.className='bb-button bb-recommend';
    input.addEventListener('click', function()
    {
        console.log('new activity!');
        new MozActivity({
            name: "share",
            data: {
                type: "url",
                number: 1,
                url: item.download
            }    
        });
    });
    input.innerHTML=translate('item_share');
    div_url.appendChild(input);

    div.appendChild(div_url);
    self.apishare_selector.create('permission_container',translate('share_link'), div, []);
};
