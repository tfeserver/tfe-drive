var Gallery = function()
{
};


Gallery.prototype.open_gallery_fetched = function(item)
{
    var self=this;
    if(self.selector)
    {
        self.selector.close();
    }
    self.selector = new Selector();
    var li = document.querySelector('.files_container li[data-id="'+item.id.replace(/"/g,'\\"')+'"]');
    var next = li.nextSibling;
    while(next && (!files.items[next.getAttribute('data-id')] || !/^image/.test(files.items[next.getAttribute('data-id')].mime)))
    {
        next=next.nextSibling;
    }
    if(!next) { next=li; }

    var previous = li.previousSibling;
    while(previous && (!files.items[previous.getAttribute('data-id')] || !/^image/.test(files.items[previous.getAttribute('data-id')].mime)))
    {
        previous=previous.previousSibling;
    }
    if(!previous) { previous=li; }

    self.zoom_level=100;
    self.pos_x=50;
    self.pos_y=50;

    var items=[];
    next= files.items[next.getAttribute('data-id')];
    previous= files.items[previous.getAttribute('data-id')];

    items.push({
        'text':  '',
        'icon':  'arrow-left previous_gallery',
        'autoclose': false,
        'callback':  self.open_gallery.bind(self, previous)
    });
    items.push({
        'text':  '',
        'icon':  'search-minus zoomout_gallery',
        'autoclose': false,
        'callback':  self.zoomout.bind(self)
    });
    items.push({
        'text':  '',
        'icon':  'search-plus zoomin_gallery',
        'autoclose': false,
        'callback':  self.zoomin.bind(self)
    });
    items.push({
        'text':  '',
        'icon':  'arrow-right next_gallery',
        'autoclose': false,
        'callback':  self.open_gallery.bind(self, next)
    });

    self.big_image= document.createElement('div');
    self.big_image.className='loading';
    self.big_image.id='big_image';

    self.selector.create(
            'selector_gallery',
            translate('gallery')+' - '+item.name,
            self.big_image,
            items
    );


    self.big_image.addEventListener('touchstart', self.movestart.bind(self));
    self.big_image.addEventListener('touchmove', self.move.bind(self));

    self.image_loaded=false;
    sdcard.cache_image('big'+item.realid, item.bigthumbnail||item.thumbnail)
    .then(function(content)
    {
        var big = document.getElementById('big_image');
        var img =new Image;
        img.onload=function()
        {
            self.image_loaded=true;
            self.container_width = big.offsetWidth;
            self.container_height = big.offsetHeight;
            self.image_width = this.width;
            self.image_height = this.height;
            big.classList.remove('loading');
            big.style.backgroundImage='url('+content+')';
        };
        img.src=content;
    }, function()
    {
        var big = document.getElementById('big_image');
        big.classList.remove('loading');
    });

    // Preload next and previous
    if(next.bigthumbnail||next.thumbnail)
    {
        sdcard.cache_image('big'+next.realid, next.bigthumbnail||next.thumbnail);
    }
    if(previous.bigthumbnail||previous.thumbnail)
    {
        sdcard.cache_image('big'+previous.realid, previous.bigthumbnail||previous.thumbnail);
    }
};

Gallery.prototype.open_gallery = function(item)
{
    var self=this;

    // If we already have the image fetched
    sdcard.has_cache_image('big'+item.realid, item.bigthumbnail||item.thumbnail).then(function()
    {
        self.open_gallery_fetched(item);
    }, function()
    {
        if(online.online && files.api.file_info)
        {
            files.alert(translate('get_file_info'));
            files.api.file_info(item.id).then(function(item) { files.alert_hide(); self.open_gallery_fetched(item) });
        }
        else
        {
            self.open_gallery_fetched(item);
        }
    });
};

Gallery.prototype.movestart = function(evt)
{
    if(!evt.changedTouches || evt.changedTouches.length===0) { return; }
    this.swipe_x = evt.changedTouches[0].clientX;
    this.swipe_y = evt.changedTouches[0].clientY;

    this.pos_x_start = this.pos_x;
    this.pos_y_start = this.pos_y;
};

Gallery.prototype.move = function(evt)
{
    if(!evt.changedTouches || evt.changedTouches.length===0) { return; }

    var dest_x = evt.changedTouches[0].clientX;
    var dest_y = evt.changedTouches[0].clientY;

    var ratio = 1 / this.big_image.offsetHeight * 100 / this.zoom_level*200;
    var percent_x = (this.swipe_x - dest_x) * ratio;
    var percent_y = (this.swipe_y - dest_y) * ratio;

    this.pos_x= this.pos_x_start+ percent_x;
    this.pos_y = this.pos_y_start+ percent_y;

    this.pos_x = Math.min(100, Math.max(0, this.pos_x));
    this.pos_y = Math.min(100, Math.max(0, this.pos_y));

    var image_percent = (gallery.container_width * gallery.zoom_level/100 ) / gallery.image_width;
    var dest_width = this.image_width * image_percent;
    var dest_height = this.image_height * image_percent;
    if(dest_height <= this.container_height)
    {
        this.pos_y = 50;
    }
    if(dest_width <= this.container_width)
    {
        this.pos_x = 50;
    }

    this.big_image.style.backgroundPosition= this.pos_x+'% '+this.pos_y+'%';
};

Gallery.prototype.zoomin = function()
{
    this.zoom_level+=25;
    this.zoom_level=Math.min(1000, this.zoom_level);
    var big = document.getElementById('big_image');
    big.style.backgroundSize=this.zoom_level+'%';
};

Gallery.prototype.zoomout = function()
{
    this.zoom_level-=25;
    this.zoom_level=Math.max(10, this.zoom_level);
    var big = document.getElementById('big_image');
    big.style.backgroundSize=this.zoom_level+'%';
};
