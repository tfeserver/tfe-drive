var Notif = function()
{
    this.init = function()
    {
        if (Notification.permission !== "granted") {
            Notification.requestPermission(function() {});
        }
    };

    this.send = function(title, body, group)
    {
        if (settings.getViewNotif() && document.hidden && Notification.permission === "granted")
        {
            var n = new Notification(title, {
                body: body,
                tag: group || 'Tfe Drive',
                icon: location.href.replace('index.html','')+'/img/icons/icon60x60.png'
            });
            n.onclick = this.receive.bind(this,n);
        }
    };

    this.receive = function(n)
    {
        n.close();
        navigator.mozApps.getSelf().onsuccess = function() { this.result.launch(); };
    };
};

