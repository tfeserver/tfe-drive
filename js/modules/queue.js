var Queue = function()
{
    var self=this;
    this.done = [];
    this.idx=0;
};

Queue.prototype.bind=function()
{
    this.goto_queue = document.querySelector('.goto_queue');
    this.goto_queue.addEventListener('click', this.open.bind(this));
    this.clear_next();
};

Queue.prototype.open=function()
{
    var self=this;
    if(this.open_selector && !this.open_selector.closed)
    {
        this.open_selector.close();
    }
    this.open_selector = new Selector();

    var div = document.createElement('div');
    var p;


    var clearall = document.createElement('p');
    clearall.className='clear_next';
    var b = document.createElement('button');
    b.className='bb-button bb-delete';
    b.innerHTML=translate('queue_clear_next');
    b.addEventListener('click', function()
    {
        if((self.queue.length>0 || self.current) && confirm(translate('queue_clear_confirm')))
        {
            self.clear_next();
            self.open_selector.close();
            self.open();
        }
    });
    clearall.appendChild(b);
    div.appendChild(clearall);


    // CURRENT ITEM
    p = document.createElement('p');
    p.className='queue_section';
    p.innerHTML=translate('queue_current_item');
    div.appendChild(p);

    var ul_current = document.createElement('ul');
    ul_current.className='queue_current';
    if(this.current)
    {
        this.addline(this.current, ul_current);
    }
    else
    {
        p.innerHTML  = '-';
    }
    div.appendChild(ul_current);

    p = document.createElement('p');
    p.className='queue_section';
    p.innerHTML=translate('queue_next_items')+' (<span class="queue_next_num">'+this.queue.length+'</span>)';
    div.appendChild(p);

    var ul_next = document.createElement('ul');
    ul_next.className='queue_next';
    if(this.queue.length)
    {
        this.queue.forEach(function(item)
        {
            self.addline(item, ul_next);
        });
    }
    div.appendChild(ul_next);

    p = document.createElement('p');
    p.innerHTML=translate('queue_done_items');
    p.className='queue_section';
    div.appendChild(p);

    var ul_done = document.createElement('ul_done');
    ul_done.className='queue_done';
    if(this.done.length)
    {
        this.done.forEach(function(item)
        {
            self.addline(item, ul_done, true);
            ul_done.appendChild(li);
        });
    }
    div.appendChild(ul_done);

    this.open_selector.create(
            'queue_selector',
            translate('queue_title'),
            div,
            []);
};

Queue.prototype.update=function()
{
    if(this.open_selector && !this.open_selector.closed)
    {
        document.querySelector('.queue_next_num').innerHTML = this.queue.length;
    }
};

Queue.prototype.add_current=function(item)
{
    this.current=item;
    var ul =   document.querySelector('.queue_current');
    if(!ul) { return; }

    ul.innerHTML='';

    if(this.open_selector && !this.open_selector.closed)
    {
        if(item)
        {
            this.addline(item, document.querySelector('.queue_current'));
            this.update();
        }
        else
        {
            this.update();
        }
    }
};

Queue.prototype.add=function(api,data)
{
    files.alert(translate('queue_added'));
    this.idx++;

    this.queue.push({
            id: this.idx,
            action: (data[0]=='create'  ? 'upload' : 'download'),
            api_type: api.type,
            api_account: api.account,
            data: data,
            from: (data[0]=='create' ? data[2].name : data[1].name),
            to: (data[0]=='create' ? data[3] : data[2])
    });
    if(!this.running)
    {
        this.run();
    }
};

Queue.prototype.add_done=function(item, status)
{
    var self=this;
    this.done.unshift({ action:item.action, from: item.from, to:item.to, status: status});
    if(!status)
    {
        this.error=true;
    }

    // Limit done items to last 100 items
    this.done = this.done.slice(-50);

    if(this.open_selector && !this.open_selector.closed)
    {
        document.querySelector('.queue_current').innerHTML = '-';

        var done = document.querySelector('.queue_done');
        item.status = status;
        self.addline(item, done, true);
        this.update();
    }
};


Queue.prototype.run = function()
{
    var self=this;
    var item = this.queue.shift();
    this.goto_queue.classList.add('active');

    this.add_current(item);

    if(item)
    {
        this.running=true;
        var api = new window[item.api_type]();
        var action = item.data.shift();
        api.initDb().then(function()
        {
            api.account = item.api_account;

            api.setAccount(item.api_account.id)
                .then(function() {
                    api[action].apply(api,item.data).then(function()
                    {
                        if(api.deco)
                        {
                            api.deco();
                        }

                        self.add_done(item, true);
                        self.run();
                    }, function()
                    {
                        console.error('error action queue ', action, item.action);
                        files.alert(translate('queue_error'), null,item.from+' -> '+item.to);
                        self.add_done(item, false);
                        self.run();
                    });
                }, function()
                {
                    files.alert(translate('queue_error'), null,item.from+' -> '+item.to);
                    console.error('error set account queue');
                    self.add_done(item, false);
                    self.run();
                });
        }, function()
        {
            console.error('error init db queue');
            files.alert(translate('queue_error'), null,item.from+' -> '+item.to);
            self.add_done(item, false);
            self.run();
        });
    }
    else
    {
        files.update();
        sdcard.update();

        // if it was running
        if(this.running)
        {
            if(this.error)
            {
                this.error=false;
                files.alert(translate('queue_all_done_with_errors'));
            }
            else
            {
                files.alert(translate('queue_all_done'));
            }
        }

        this.running=false;
        this.goto_queue.classList.remove('active');
    }
};

Queue.prototype.clear_next = function()
{
    this.current=null;
    this.running=false;
    this.queue=[];
    this.error = false;
    this.goto_queue.classList.remove('active');
};



Queue.prototype.init = function()
{
    this.bind();
};

Queue.prototype.addline = function(item, ul, prepend)
{
    li = document.createElement('li');
    if(item.status!==undefined && item.status!==null)
    {
console.log('with status');
        li.className=item.status ? 'queue_ok' :'queue_ko';
    }
        console.log('here ',item);
    if(item.id)
    {
        li.id='queue_'+item.id;
    }

    var span_from  = document.createElement('span');
    span_from.className='queue_from';
    var from = document.createTextNode(translate('queue_from')+'  '+item.from);
    span_from.appendChild(from);

    var span_to  = document.createElement('span');
    span_to.className='queue_to';
    var to = document.createTextNode(translate('queue_to')+' '+(item.to || '/'));
    span_to.appendChild(to);

    var span_action = document.createElement('span');
    span_action.className='queue_icon fa fa-'+item.action;

    li.appendChild(span_action);
    li.appendChild(span_from);
    li.appendChild(span_to);
    if(prepend)
    {
        ul.insertBefore(li, ul.childNodes[0]);
    }
    else
    {
        ul.appendChild(li);
    }
};



