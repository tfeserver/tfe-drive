var Selector = function()
{
};

Selector.prototype.create = function(extraclass,title, description, items)
{
    var div =document.createElement('div');
    div.classList.add('selector');
    if(extraclass)
    {
        div.classList.add(extraclass);
    }


    var h2=document.createElement('h2');
    var text = document.createTextNode(title);
    h2.appendChild(text);
    div.appendChild(h2);

    var p = document.createElement('p');
    p.className='fa fa-close selector_close';
    p.addEventListener('click', vibrate.button.bind(vibrate));
    p.addEventListener('click', this.close.bind(this));
    div.appendChild(p);

    var div_inner = document.createElement('div');
    div_inner.classList.add('selector_inner');
    div.appendChild(div_inner);



    if(description)
    {
        var div_description=document.createElement('div');
        div_description.className='description';
        if(typeof description==='string')
        {
            div_description.innerHTML=description;
        }
        else
        {
            div_description.appendChild(description);
        }
        div_inner.appendChild(div_description);
    }


    // Simple menu buttons
    if(Array.isArray(items))
    {
        var ul = document.createElement('ul');
        div_inner.appendChild(ul);
        if(items.length>0)
        {
            for(var i=0; i<items.length; i++)
            {
                var li = document.createElement('li');
                li.innerHTML= items[i].text;
                li.className='fa fa-'+items[i].icon;
                li.addEventListener('click', vibrate.button.bind(vibrate));
                li.addEventListener('click', items[i].callback);
                if(items[i].autoclose!==false)
                {
                    li.addEventListener('click', this.close.bind(this));
                }
                ul.appendChild(li);
            }
        }
    }
    // Menu buttons with sections
    else
    {
        for(var section in items)
        {
            if(items[section].length>0)
            {
                var p = document.createElement('p');
                p.className='menu_section_name';
                p.innerHTML=translate(section);
                div_inner.appendChild(p);

                var ul = document.createElement('ul');
                div_inner.appendChild(ul);
                for(var i=0; i<items[section].length; i++)
                {
                    var li = document.createElement('li');
                    li.innerHTML= items[section][i].text;
                    li.className='fa fa-'+items[section][i].icon;
                    li.addEventListener('click', vibrate.button.bind(vibrate));
                    li.addEventListener('click', items[section][i].callback);
                    if(items[section][i].autoclose!==false)
                    {
                        li.addEventListener('click', this.close.bind(this));
                    }
                    ul.appendChild(li);
                }
            }
        }
    }

    document.body.appendChild(div);

    this.container = div;
};

Selector.prototype.create_picker = function(params)
{
    var self=this;
    var title = params.title;
    var root = params.root;
    var callback = params.callback;
    var extra = params.extra;
    var dir_callback = params.dir_callback;

    var p, span, text;

    var div =document.createElement('div');
    div.classList.add('selector');
    div.classList.add('selector_picker');


    var h2=document.createElement('h2');
    text = document.createTextNode(title);
    h2.appendChild(text);
    div.appendChild(h2);

    p = document.createElement('p');
    p.className='fa fa-close selector_close';
    p.addEventListener('click', vibrate.button.bind(vibrate));
    p.addEventListener('click', this.close.bind(this));
    div.appendChild(p);

    var div_inner = document.createElement('div');
    div_inner.classList.add('selector_inner');
    div.appendChild(div_inner);

    if(extra)
    {
        p = document.createElement('p');
        p.className='selector_extra';
        text = document.createTextNode(extra);
        p.appendChild(text);
        div_inner.appendChild(p);
    }

    if(params.refresh_callback)
    {
        p = document.createElement('p');
        p.className='selector_refresh';
        span = document.createElement('span');
        span.className='fa fa-refresh';

        text = document.createTextNode(translate('refresh_path'));
        p.addEventListener('click', vibrate.button.bind(vibrate));
        p.addEventListener('click', function()
        {
            files.alert(translate('loading_dir_structure'));
            sdcard.update().then(function()
            {
                self.close();
                params.refresh_callback();
            }, function()
            {
                files.alert(translate('error_loading_dir_structure'));
            });
        });
        p.appendChild(span);
        p.appendChild(text);
        div_inner.appendChild(p);
    }


    p = document.createElement('p');
    p.className='selector_currentpath';
    text = document.createTextNode(translate('current_path_selection')+' '+(root.path || '/'));
    p.appendChild(text);
    div_inner.appendChild(p);



    p = document.createElement('p');
    p.className='selector_select';
    text = document.createTextNode(translate('select_this_directory'));
    p.addEventListener('click', vibrate.button.bind(vibrate));
    p.addEventListener('click', this.select_picker.bind(this, callback, root));
    span = document.createElement('span');
    span.className='fa fa-arrow-right selector_pick';
    p.appendChild(span);
    p.appendChild(text);
    div_inner.appendChild(p);



    var ul = document.createElement('ul');
    div_inner.appendChild(ul);

    if(root.parent)
    {
        var li = document.createElement('li');
        text = document.createTextNode('..');
        var new_params = clone(params);
        new_params.root=root.parent;
        li.addEventListener('click', vibrate.button.bind(vibrate));
        li.addEventListener('click', this.close.bind(this));
        li.addEventListener('click', this.create_picker.bind(this, new_params));
        li.appendChild(text);
        li.className='fa fa-folder';
        ul.appendChild(li);
    }

    var sorted_dirs=[];
    if(root.dirs)
    {
        sorted_dirs= Object.keys(root.dirs);
        sorted_dirs.sort(function(a,b)
        {
            var aname = a.toUpperCase();
            var bname = b.toUpperCase();

            if(aname > bname) return 1;
            if(aname < bname) return -1;
            return 0;
        });
    }

    sorted_dirs.forEach(function(dir)
    {
        var li = document.createElement('li');
        text = document.createTextNode(dir);
        li.appendChild(text);
        li.className='fa fa-folder';
        if(root.dirs[dir].parent)
        {
            li.addEventListener('click',vibrate.button.bind(vibrate));
            li.addEventListener('click',(function(dir)
            {
                if(root.dirs[dir].dirs===null)
                {
                    root.dirs[dir].dirs={};
                    files.alert(translate('loading_dir_structure'), 99999999);
                    dir_callback(root.dirs[dir].id, root.dirs[dir].path, root.dirs[dir]).then(function(dirs)
                    {
                        files.alert_hide();
                        params.root= dirs;
                        self.close.call(self);
                        self.create_picker.call(self, params);
                    }, function()
                    {
                        alert('error getting sub dir');
                    });
                }
                else
                {
                    params.root= root.dirs[dir];
                    self.close.call(self);
                    self.create_picker.call(self, params);
                }
            }).bind(this,dir));
        }
        ul.appendChild(li);
    });
    if(params.refresh_callback)
    {
        var li = document.createElement('li');
        text = document.createTextNode(translate('new_folder'));
        li.appendChild(text);
        li.className='fa fa-plus';

        li.addEventListener('click',vibrate.button.bind(vibrate));
        li.addEventListener('click',function()
        {
            console.log('root = ',root);
            var newname = prompt(translate('enter_new_name'));
            if(newname)
            {
                root.dirs[newname] = {
                    path: root.path+newname+'/',
                    dirs: {},
                    parent: root
                };
                var blob = new Blob( [ '' ], { type: 'text/plain'} );
                sdcard.add(blob, root.dirs[newname].path+'.empty', true).then(function()
                {
                    params.root= root.dirs[newname];
                    self.close.call(self);
                    self.create_picker.call(self, params);
                });
            }
        });
        ul.appendChild(li);
    }

    document.body.appendChild(div);

    this.container = div;
};
Selector.prototype.select_picker = function(callback, root, e)
{
    if(e.preventDefault) { e.preventDefault(); }
    if(e.stopPropagation) { e.stopPropagation(); }
    callback(root);
    this.close();
};

Selector.prototype.close = function()
{
    this.closed=true;
    if(this.container.parentNode)
    {
        this.container.parentNode.removeChild(this.container);
    }
};


